function id( el ){
    //return document.getElementById( el );
    return $( el );
}
function calcTotal( un01, qnt01 )
{
    return un01 * qnt01;
}
function getElementParent(event){
return event.srcElement.parentNode.parentNode.getAttribute('id');
}
function getValorUnitario(elParent){
return $('#'+elParent+' .class_unit input').val();
}
function getQuantidade(elParent){
return $('#'+elParent+' .class_quant input').val();
}
function setFieldTotal(elParent, valueUnit, valueQuant){

id('#'+elParent+' .class_total input').val(calcTotal( valueUnit , valueQuant));
setTotalFinal();
}
function setTotalFinal(){

var total = 0;
$('#table-shop .class_total input').each(function(){
    if(this.value != ''){
    var valor = this.value;
    total += parseInt(valor);
    }
});
$('#total .value_total').html(total);
$('#total .value_total').val(total);
}
$(document).ready(function(){
    id('#table-shop .class_unit').keyup(function(event)
    {
        var elemenPai = getElementParent(event);
        var valueUnit = getValorUnitario(elemenPai);
        var valueQuant = getQuantidade(elemenPai);

        setFieldTotal(elemenPai, valueUnit , valueQuant);
    });      
  
    id('#table-shop .class_quant').keyup(function(event)
    {
        var elemenPai = getElementParent(event);
        var valueUnit = getValorUnitario(elemenPai);
        var valueQuant = getQuantidade(elemenPai);

        setFieldTotal(elemenPai, valueUnit , valueQuant);
    });

    $("#qnt01").val(0);
    $("#qnt02").val(0);
    $("#qnt03").val(0);
    $("#qnt04").val(0);
    $("#qnt05").val(0);
    $("#qnt06").val(0);
    $("#qnt07").val(0);
    $("#qnt08").val(0);
    $("#qnt09").val(0);
    $("#qnt10").val(0);
    $("#qnt11").val(0);
    $("#qnt12").val(0);
    $("#qnt13").val(0);
    $("#qnt14").val(0);
    $("#qnt15").val(0);
    $("#qnt16").val(0);
    $("#qnt17").val(0);
    $("#qnt18").val(0);

    $("#aumentaItem1").click(function () {
        var input = $("#qnt01")[0];
        input.value = parseInt(input.value, 10) + 1;

        var elemenPai = getElementParent(event);
        var valueUnit = getValorUnitario(elemenPai);
        var valueQuant = getQuantidade(elemenPai);

        setFieldTotal(elemenPai, valueUnit , valueQuant);
    });

    $("#diminuiItem1").click(function () {
        var input = $("#qnt01")[0];
        var decrescimo = parseInt(input.value, 10) - 1;
        input.value = decrescimo < 1 ? 0 : decrescimo;

        var elemenPai = getElementParent(event);
        var valueUnit = getValorUnitario(elemenPai);
        var valueQuant = getQuantidade(elemenPai);

        setFieldTotal(elemenPai, valueUnit , valueQuant);
    });

    $("#aumentaItem2").click(function () {
        var input = $("#qnt02")[0];
        input.value = parseInt(input.value, 10) + 1;

        var elemenPai = getElementParent(event);
        var valueUnit = getValorUnitario(elemenPai);
        var valueQuant = getQuantidade(elemenPai);

        setFieldTotal(elemenPai, valueUnit , valueQuant);
    });

    $("#diminuiItem2").click(function () {
        var input = $("#qnt02")[0];
        var decrescimo = parseInt(input.value, 10) - 1;
        input.value = decrescimo < 1 ? 0 : decrescimo;

        var elemenPai = getElementParent(event);
        var valueUnit = getValorUnitario(elemenPai);
        var valueQuant = getQuantidade(elemenPai);

        setFieldTotal(elemenPai, valueUnit , valueQuant);
    });

    $("#aumentaItem3").click(function () {
        var input = $("#qnt03")[0];
        input.value = parseInt(input.value, 10) + 1;

        var elemenPai = getElementParent(event);
        var valueUnit = getValorUnitario(elemenPai);
        var valueQuant = getQuantidade(elemenPai);

        setFieldTotal(elemenPai, valueUnit , valueQuant);
    });

    $("#diminuiItem3").click(function () {
        var input = $("#qnt03")[0];
        var decrescimo = parseInt(input.value, 10) - 1;
        input.value = decrescimo < 1 ? 0 : decrescimo;

        var elemenPai = getElementParent(event);
        var valueUnit = getValorUnitario(elemenPai);
        var valueQuant = getQuantidade(elemenPai);

        setFieldTotal(elemenPai, valueUnit , valueQuant);
    });

     $("#aumentaItem4").click(function () {
        var input = $("#qnt04")[0];
        input.value = parseInt(input.value, 10) + 1;

        var elemenPai = getElementParent(event);
        var valueUnit = getValorUnitario(elemenPai);
        var valueQuant = getQuantidade(elemenPai);

        setFieldTotal(elemenPai, valueUnit , valueQuant);
    });

    $("#diminuiItem4").click(function () {
        var input = $("#qnt04")[0];
        var decrescimo = parseInt(input.value, 10) - 1;
        input.value = decrescimo < 1 ? 0 : decrescimo;

        var elemenPai = getElementParent(event);
        var valueUnit = getValorUnitario(elemenPai);
        var valueQuant = getQuantidade(elemenPai);

        setFieldTotal(elemenPai, valueUnit , valueQuant);
    });

    $("#aumentaItem5").click(function () {
        var input = $("#qnt05")[0];
        input.value = parseInt(input.value, 10) + 1;

        var elemenPai = getElementParent(event);
        var valueUnit = getValorUnitario(elemenPai);
        var valueQuant = getQuantidade(elemenPai);

        setFieldTotal(elemenPai, valueUnit , valueQuant);
    });

    $("#diminuiItem5").click(function () {
        var input = $("#qnt05")[0];
        var decrescimo = parseInt(input.value, 10) - 1;
        input.value = decrescimo < 1 ? 0 : decrescimo;

        var elemenPai = getElementParent(event);
        var valueUnit = getValorUnitario(elemenPai);
        var valueQuant = getQuantidade(elemenPai);

        setFieldTotal(elemenPai, valueUnit , valueQuant);
    });

    $("#aumentaItem6").click(function () {
        var input = $("#qnt06")[0];
        input.value = parseInt(input.value, 10) + 1;

        var elemenPai = getElementParent(event);
        var valueUnit = getValorUnitario(elemenPai);
        var valueQuant = getQuantidade(elemenPai);

        setFieldTotal(elemenPai, valueUnit , valueQuant);
    });

    $("#diminuiItem6").click(function () {
        var input = $("#qnt06")[0];
        var decrescimo = parseInt(input.value, 10) - 1;
        input.value = decrescimo < 1 ? 0 : decrescimo;

        var elemenPai = getElementParent(event);
        var valueUnit = getValorUnitario(elemenPai);
        var valueQuant = getQuantidade(elemenPai);

        setFieldTotal(elemenPai, valueUnit , valueQuant);
    });

    $("#aumentaItem7").click(function () {
        var input = $("#qnt07")[0];
        input.value = parseInt(input.value, 10) + 1;

        var elemenPai = getElementParent(event);
        var valueUnit = getValorUnitario(elemenPai);
        var valueQuant = getQuantidade(elemenPai);

        setFieldTotal(elemenPai, valueUnit , valueQuant);
    });

    $("#diminuiItem7").click(function () {
        var input = $("#qnt07")[0];
        var decrescimo = parseInt(input.value, 10) - 1;
        input.value = decrescimo < 1 ? 0 : decrescimo;

        var elemenPai = getElementParent(event);
        var valueUnit = getValorUnitario(elemenPai);
        var valueQuant = getQuantidade(elemenPai);

        setFieldTotal(elemenPai, valueUnit , valueQuant);
    });

    $("#aumentaItem8").click(function () {
        var input = $("#qnt08")[0];
        input.value = parseInt(input.value, 10) + 1;

        var elemenPai = getElementParent(event);
        var valueUnit = getValorUnitario(elemenPai);
        var valueQuant = getQuantidade(elemenPai);

        setFieldTotal(elemenPai, valueUnit , valueQuant);
    });

    $("#diminuiItem8").click(function () {
        var input = $("#qnt08")[0];
        var decrescimo = parseInt(input.value, 10) - 1;
        input.value = decrescimo < 1 ? 0 : decrescimo;

        var elemenPai = getElementParent(event);
        var valueUnit = getValorUnitario(elemenPai);
        var valueQuant = getQuantidade(elemenPai);

        setFieldTotal(elemenPai, valueUnit , valueQuant);
    });

    $("#aumentaItem9").click(function () {
        var input = $("#qnt09")[0];
        input.value = parseInt(input.value, 10) + 1;

        var elemenPai = getElementParent(event);
        var valueUnit = getValorUnitario(elemenPai);
        var valueQuant = getQuantidade(elemenPai);

        setFieldTotal(elemenPai, valueUnit , valueQuant);
    });

    $("#diminuiItem9").click(function () {
        var input = $("#qnt09")[0];
        var decrescimo = parseInt(input.value, 10) - 1;
        input.value = decrescimo < 1 ? 0 : decrescimo;

        var elemenPai = getElementParent(event);
        var valueUnit = getValorUnitario(elemenPai);
        var valueQuant = getQuantidade(elemenPai);

        setFieldTotal(elemenPai, valueUnit , valueQuant);
    });
    
    $("#aumentaItem10").click(function () {
        var input = $("#qnt10")[0];
        input.value = parseInt(input.value, 10) + 1;

        var elemenPai = getElementParent(event);
        var valueUnit = getValorUnitario(elemenPai);
        var valueQuant = getQuantidade(elemenPai);

        setFieldTotal(elemenPai, valueUnit , valueQuant);
    });

    $("#diminuiItem10").click(function () {
        var input = $("#qnt10")[0];
        var decrescimo = parseInt(input.value, 10) - 1;
        input.value = decrescimo < 1 ? 0 : decrescimo;

        var elemenPai = getElementParent(event);
        var valueUnit = getValorUnitario(elemenPai);
        var valueQuant = getQuantidade(elemenPai);

        setFieldTotal(elemenPai, valueUnit , valueQuant);
    });

    $("#aumentaItem11").click(function () {
        var input = $("#qnt11")[0];
        input.value = parseInt(input.value, 10) + 1;

        var elemenPai = getElementParent(event);
        var valueUnit = getValorUnitario(elemenPai);
        var valueQuant = getQuantidade(elemenPai);

        setFieldTotal(elemenPai, valueUnit , valueQuant);
    });

    $("#diminuiItem11").click(function () {
        var input = $("#qnt11")[0];
        var decrescimo = parseInt(input.value, 10) - 1;
        input.value = decrescimo < 1 ? 0 : decrescimo;

        var elemenPai = getElementParent(event);
        var valueUnit = getValorUnitario(elemenPai);
        var valueQuant = getQuantidade(elemenPai);

        setFieldTotal(elemenPai, valueUnit , valueQuant);
    });

    $("#aumentaItem12").click(function () {
        var input = $("#qnt12")[0];
        input.value = parseInt(input.value, 10) + 1;

        var elemenPai = getElementParent(event);
        var valueUnit = getValorUnitario(elemenPai);
        var valueQuant = getQuantidade(elemenPai);

        setFieldTotal(elemenPai, valueUnit , valueQuant);
    });

    $("#diminuiItem12").click(function () {
        var input = $("#qnt12")[0];
        var decrescimo = parseInt(input.value, 10) - 1;
        input.value = decrescimo < 1 ? 0 : decrescimo;

        var elemenPai = getElementParent(event);
        var valueUnit = getValorUnitario(elemenPai);
        var valueQuant = getQuantidade(elemenPai);

        setFieldTotal(elemenPai, valueUnit , valueQuant);
    });

    $("#aumentaItem13").click(function () {
        var input = $("#qnt13")[0];
        input.value = parseInt(input.value, 10) + 1;

        var elemenPai = getElementParent(event);
        var valueUnit = getValorUnitario(elemenPai);
        var valueQuant = getQuantidade(elemenPai);

        setFieldTotal(elemenPai, valueUnit , valueQuant);
    });

    $("#diminuiItem13").click(function () {
        var input = $("#qnt13")[0];
        var decrescimo = parseInt(input.value, 10) - 1;
        input.value = decrescimo < 1 ? 0 : decrescimo;

        var elemenPai = getElementParent(event);
        var valueUnit = getValorUnitario(elemenPai);
        var valueQuant = getQuantidade(elemenPai);

        setFieldTotal(elemenPai, valueUnit , valueQuant);
    });

     $("#aumentaItem14").click(function () {
        var input = $("#qnt14")[0];
        input.value = parseInt(input.value, 10) + 1;

        var elemenPai = getElementParent(event);
        var valueUnit = getValorUnitario(elemenPai);
        var valueQuant = getQuantidade(elemenPai);

        setFieldTotal(elemenPai, valueUnit , valueQuant);
    });

    $("#diminuiItem14").click(function () {
        var input = $("#qnt14")[0];
        var decrescimo = parseInt(input.value, 10) - 1;
        input.value = decrescimo < 1 ? 0 : decrescimo;

        var elemenPai = getElementParent(event);
        var valueUnit = getValorUnitario(elemenPai);
        var valueQuant = getQuantidade(elemenPai);

        setFieldTotal(elemenPai, valueUnit , valueQuant);
    });

    $("#aumentaItem15").click(function () {
        var input = $("#qnt15")[0];
        input.value = parseInt(input.value, 10) + 1;

        var elemenPai = getElementParent(event);
        var valueUnit = getValorUnitario(elemenPai);
        var valueQuant = getQuantidade(elemenPai);

        setFieldTotal(elemenPai, valueUnit , valueQuant);
    });

    $("#diminuiItem15").click(function () {
        var input = $("#qnt15")[0];
        var decrescimo = parseInt(input.value, 10) - 1;
        input.value = decrescimo < 1 ? 0 : decrescimo;

        var elemenPai = getElementParent(event);
        var valueUnit = getValorUnitario(elemenPai);
        var valueQuant = getQuantidade(elemenPai);

        setFieldTotal(elemenPai, valueUnit , valueQuant);
    });

    $("#aumentaItem16").click(function () {
        var input = $("#qnt16")[0];
        input.value = parseInt(input.value, 10) + 1;

        var elemenPai = getElementParent(event);
        var valueUnit = getValorUnitario(elemenPai);
        var valueQuant = getQuantidade(elemenPai);

        setFieldTotal(elemenPai, valueUnit , valueQuant);
    });

    $("#diminuiItem16").click(function () {
        var input = $("#qnt16")[0];
        var decrescimo = parseInt(input.value, 10) - 1;
        input.value = decrescimo < 1 ? 0 : decrescimo;

        var elemenPai = getElementParent(event);
        var valueUnit = getValorUnitario(elemenPai);
        var valueQuant = getQuantidade(elemenPai);

        setFieldTotal(elemenPai, valueUnit , valueQuant);
    });

    $("#aumentaItem17").click(function () {
        var input = $("#qnt17")[0];
        input.value = parseInt(input.value, 10) + 1;

        var elemenPai = getElementParent(event);
        var valueUnit = getValorUnitario(elemenPai);
        var valueQuant = getQuantidade(elemenPai);

        setFieldTotal(elemenPai, valueUnit , valueQuant);
    });

    $("#diminuiItem17").click(function () {
        var input = $("#qnt17")[0];
        var decrescimo = parseInt(input.value, 10) - 1;
        input.value = decrescimo < 1 ? 0 : decrescimo;

        var elemenPai = getElementParent(event);
        var valueUnit = getValorUnitario(elemenPai);
        var valueQuant = getQuantidade(elemenPai);

        setFieldTotal(elemenPai, valueUnit , valueQuant);
    });

    $("#aumentaItem18").click(function () {
        var input = $("#qnt18")[0];
        input.value = parseInt(input.value, 10) + 1;

        var elemenPai = getElementParent(event);
        var valueUnit = getValorUnitario(elemenPai);
        var valueQuant = getQuantidade(elemenPai);

        setFieldTotal(elemenPai, valueUnit , valueQuant);
    });

    $("#diminuiItem18").click(function () {
        var input = $("#qnt18")[0];
        var decrescimo = parseInt(input.value, 10) - 1;
        input.value = decrescimo < 1 ? 0 : decrescimo;

        var elemenPai = getElementParent(event);
        var valueUnit = getValorUnitario(elemenPai);
        var valueQuant = getQuantidade(elemenPai);

        setFieldTotal(elemenPai, valueUnit , valueQuant);
    });

});