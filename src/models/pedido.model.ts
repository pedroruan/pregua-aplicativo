import { Produto } from '../models/produto.model';

export interface Pedido {
    produto: Produto;
    quantidade: number;
}