import { Pedido } from '../models/pedido.model';
import { DividirConta } from '../models/dividir.conta.model';

export interface Fechamento {
    _id: string;
    pedidos: Pedido[];
    quantidadeItens: number;
    subTotal:number;
    valorTotal: number;
    formaPagamento: string;
    valorPago: number;
    valorTroco: number;
    divisaoContas: DividirConta[];
    numeroContas: number;
    
    caixinha: boolean;
    valorCaixinha: number;

    desconto: boolean;
    valorDesconto: number;

}