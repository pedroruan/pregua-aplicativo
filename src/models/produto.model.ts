export interface Produto {
    _id: string;
    address: string;
    desconto: number;
    descricao: string;
    name: string;
    valor: number;
}