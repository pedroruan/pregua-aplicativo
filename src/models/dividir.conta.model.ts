export interface DividirConta {
    valorConta:number;
    valorPago: number;
    valorTroco: number;
    formaPagamento: string;
    valorDesconto:number;
    valorCaixinha:number;
}