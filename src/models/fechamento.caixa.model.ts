export interface FechamentoCaixaModel {
    valorDebito: number;
    valorCredito: number;
    valorDinheiro: number;
    qtdDebito: number;
    qtdCredito: number;
    qtdDinheiro: number;
    valorMedio: number;
    valorTotal: number;
}