export interface Company {
    _id: string;
    name: string;
    photo: string;
    endereco: string;
}