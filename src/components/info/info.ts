import { Component, Input } from '@angular/core';
import { Company } from '../../models/company.model';

/**
 * Generated class for the InfoComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'info',
  templateUrl: 'info.html'
})
export class InfoComponent {

  @Input()
  company: Company;

  constructor() {
  }

}
