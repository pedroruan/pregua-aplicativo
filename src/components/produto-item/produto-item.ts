import { Component, Input, Host } from '@angular/core';
import { Pedido } from '../../models/pedido.model';
import { BasePage } from '../../pages/base-page';
import { Produto } from '../../models/produto.model';
import { HomePage } from '../../pages/home/home';

@Component({
  selector: 'produto-item',
  templateUrl: 'produto-item.html'
})
export class ProdutoItemComponent{

  @Input("produto") produto:Produto;
  pedido:Pedido;
  i: number;

  constructor(@Host() public home: HomePage) {
  }

  ngOnInit(){
    this.pedido = this.home.getPedido(this.produto);
  }

  add() {
    this.pedido.quantidade++;
    this.home.fechamento.quantidadeItens++;
    this.home.fechamento.valorTotal += (this.produto.valor * 1);

    // this.home.fechamento.pedidos.find((pedido:Pedido)=>{ return pedido.produto._id == this.produto._id; }).quantidade = this.pedido.quantidade;

    // if(this.home.fechamento.pedidos.filter((pedido:Pedido)=>{ return pedido.produto._id == p._id; }).length > 0){
    //   this.home.fechamento.pedidos.map((pedido:Pedido)=>{
    //     if(p._id == pedido.produto._id){
          
    //     }
    //   });
    // }else{
    //   this.pedido.quantidade++;
    //   this.home.fechamento.pedidos.push(pedido);
    //   this.home.fechamento.quantidadeItens++;
    //   this.home.fechamento.valorTotal += pedido.produto.valor;
    // }
  }

  subtract() {
    if(this.pedido.quantidade > 0){
      this.pedido.quantidade--;
      this.home.fechamento.quantidadeItens--;
      this.home.fechamento.valorTotal -= (this.produto.valor * 1);
    }
      // if(this.home.fechamento.pedidos.filter((pedido:Pedido)=>{ return pedido.produto._id == p._id; }).length > 0){
    //   this.home.fechamento.pedidos.map((pedido:Pedido)=>{
    //     if(p._id == pedido.produto._id){
    //       pedido.quantidade--;
    //       this.home.fechamento.quantidadeItens--;
    //       this.home.fechamento.valorTotal -= pedido.produto.valor;
    //     }
    //   });
    // }
  }

}
