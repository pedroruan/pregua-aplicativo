import { Component } from '@angular/core';
import { MenuController } from 'ionic-angular/components/app/menu-controller';

/**
 * Generated class for the HeaderComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'app-header',
  templateUrl: 'header.html'
})
export class HeaderComponent {

  constructor(public menuCtrl:MenuController) {
  }
  openMenu() {
    this.menuCtrl.open();
  }

}
