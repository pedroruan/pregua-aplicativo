import { NgModule } from '@angular/core';
import { InfoComponent } from './info/info';
import { ProdutoItemComponent } from './produto-item/produto-item';
@NgModule({
	declarations: [InfoComponent,
    ProdutoItemComponent],
	imports: [],
	exports: [InfoComponent,
    ProdutoItemComponent]
})
export class ComponentsModule {}
