import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController, ToastController, ViewController } from 'ionic-angular';
import { DividirConta } from '../../models/dividir.conta.model';
import { BasePage } from '../base-page';

@Component({
  selector: 'page-pagamento-modal',
  templateUrl: 'pagamento-modal.html',
})
export class PagamentoModalPage extends BasePage {
  conta: DividirConta;
  formaPagamento: string;
  mensagemAjuste: string;

  constructor(
    public viewCtrl: ViewController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController, ) {
    super(navCtrl, navParams, alertCtrl, loadingCtrl, toastCtrl);
    this.conta = navParams.get('conta');
    this.formaPagamento = navParams.get('formaPagamento');
    
    if(!this.conta.valorPago){
      this.conta.valorPago = this.conta.valorConta;
    }

    if(this.formaPagamento != this.conta.formaPagamento){
      this.conta.valorPago = this.conta.valorConta;
      this.conta.valorTroco = 0;
    }

    if(!this.conta.valorTroco){
      this.calculaTroco();
    }else{
      this.verificaDescontoOuCaixinha();
    }
  }

  ionViewDidLoad() {
  }

  calculaTroco() {
    if (this.formaPagamento == 'dinheiro') {
      if (this.conta.valorPago >= this.conta.valorConta) {
        this.conta.valorTroco = this.round( (this.conta.valorPago - this.conta.valorConta), 2);
      }
    }
    this.verificaDescontoOuCaixinha();
  }

  verificaDescontoOuCaixinha() {
    let troco = this.conta.valorTroco || 0;
    troco = this.round(troco,2);
    let pagamento = this.round(this.conta.valorPago,2) - troco;
    let caixinha = 0;
    let desconto = 0;

    this.conta.valorCaixinha = caixinha;
    this.conta.valorDesconto = desconto;

    if (pagamento > this.conta.valorConta) {
      caixinha = pagamento - this.round(this.conta.valorConta,2);
      this.conta.valorCaixinha = this.round(caixinha,2);
    } else if(pagamento < this.conta.valorConta){
      desconto = this.round(this.conta.valorConta,2) - pagamento;
      this.conta.valorDesconto = this.round(desconto,2);
    }else{
      this.mensagemAjuste = null;
    }
  }

  salvar(){
    this.conta.formaPagamento = this.formaPagamento;
    this.viewCtrl.dismiss(this.conta);
  }

  cancelar(){
    this.viewCtrl.dismiss();
  }
}
