import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, ToastController, Events } from 'ionic-angular';
import { Fechamento } from '../../models/fechamento.model';
import { DividirConta } from '../../models/dividir.conta.model';
import { BasePage } from "../base-page";
import { FechamentoProvider } from "../../providers/fechamento/fechamento";
import { HomePage } from "../home/home";
import { PagamentoModalPage } from '../pagamento-modal/pagamento-modal';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';

@Component({
  selector: 'page-dividir-conta',
  templateUrl: 'dividir-conta.html',
})
export class DividirContaPage extends BasePage {

  fechamento: Fechamento;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public events: Events,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public fechamentoProvider: FechamentoProvider) {
    super(navCtrl, navParams, alertCtrl, loadingCtrl, toastCtrl);
    this.fechamento = this.navParams.get('fechamento');
    this.fechamento.divisaoContas = [];
    let i = 0;
    for (i; i < this.fechamento.numeroContas; i++) {
      this.fechamento.divisaoContas.push(<DividirConta>{});
    }
    this.dividirConta();
  }

  inserirPagamento(formaPagamento: string, conta: any) {
    let pgtoModal = this.modalCtrl.create(PagamentoModalPage, { conta: conta, formaPagamento: formaPagamento });
    pgtoModal.onDidDismiss(contaPaga => {
      if (contaPaga)
        conta = contaPaga;
    });
    pgtoModal.present();
  }

  dividirConta(idx = -1) {
    let numeroContas = this.fechamento.numeroContas;
    let valorTotal = this.round(this.fechamento.valorTotal, 2);
    let valorParcial = 0;

    for (let i = 0; i < this.fechamento.divisaoContas.length; i++) {
      let conta = this.fechamento.divisaoContas[i];
      if (conta.formaPagamento) {
        valorParcial += conta.valorConta;
        numeroContas--;
      }
    }

    if (numeroContas > 1) {
      for (let i = 0; i < this.fechamento.divisaoContas.length; i++) {
        if (!this.fechamento.divisaoContas[i].formaPagamento) {
          if (idx != i) {
            let restante = valorTotal - valorParcial;
            let valorConta = restante / ((this.fechamento.divisaoContas.length) - i);
            valorConta = this.round(valorConta, 2);
            this.fechamento.divisaoContas[i].valorConta = valorConta;
            valorParcial += valorConta;
          } else {
            let v: any = this.fechamento.divisaoContas[i].valorConta;
            this.fechamento.divisaoContas[i].valorConta = parseFloat(v);
            valorParcial += parseFloat(this.fechamento.divisaoContas[i].valorConta.toFixed(2));
          }
        }
      }

      if (valorParcial < valorTotal) {
        let last = this.findLastOpennedAccount(idx);
        this.fechamento.divisaoContas[last].valorConta += (valorTotal - valorParcial);
        this.fechamento.divisaoContas[last].valorConta = this.round(this.fechamento.divisaoContas[last].valorConta, 2);
      } else if (valorParcial > valorTotal) {
        let last = this.findLastOpennedAccount(idx);
        this.fechamento.divisaoContas[last].valorConta -= (valorParcial - valorTotal);
        this.fechamento.divisaoContas[last].valorConta = this.round(this.fechamento.divisaoContas[last].valorConta, 2);
      }
    }else{
      for (let i = 0; i < this.fechamento.divisaoContas.length; i++) {
        if (!this.fechamento.divisaoContas[i].formaPagamento) {
          this.fechamento.divisaoContas[i].valorConta = this.round((valorTotal - valorParcial),2);
        }
      }
    }
  }

  findLastOpennedAccount(idx = -1) {
    let last = (this.fechamento.divisaoContas.length);
    for (let j = last - 1; j >= 0; j--) {
      if (j != idx)
        if (!this.fechamento.divisaoContas[j].formaPagamento)
          return j;
    }
    return (last - 1);
  }

  calcularValorPago(d: DividirConta) {
    let valorPago = d.valorPago - d.valorTroco;
    return valorPago;
  }

  goToFechamento() {
    this.navCtrl.push('FechamentoPage');
  }

  selecionarFormaPagamento(f: string, i: number) {
    this.fechamento.divisaoContas[i].formaPagamento = f;
    this.fechamento.divisaoContas[i].valorPago = this.fechamento.divisaoContas[i].valorConta;
  }

  fecharConta() {
    this.fechamento.formaPagamento = 'contaDividida';
    let valorTotal = 0;
    var i = 0;
    for (i; i < this.fechamento.divisaoContas.length; i++) {
      if (!this.fechamento.divisaoContas[i].formaPagamento) {
        super.mostrarMsg('É necessário informar a forma de pagamento da Conta' + (i + 1) + '!');
        return;
      }
    }
    this.showLoading();
    this.fechamentoProvider.saveFechamento(this.fechamento).toPromise().then((r) => {
      console.log(r);
      super.mostrarMsg(r.message);
      this.events.publish('home:init');
      this.navCtrl.popToRoot();
      this.dismissLoading();
    }).catch((e) => {
      console.log(e);
      super.mostrarErro((e.error && e.error.message) ? e.error.message : e.message);
      super.dismissLoading();
    });
  }

}
