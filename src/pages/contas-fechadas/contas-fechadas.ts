import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController, ToastController } from 'ionic-angular';
import { FechamentoProvider } from '../../providers/fechamento/fechamento';
import { Observable } from 'rxjs/Observable';
import { BasePage } from '../base-page';

@Component({
  selector: 'page-contas-fechadas',
  templateUrl: 'contas-fechadas.html',
})
export class ContasFechadasPage extends BasePage{

  contas:Observable<any>;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public fechamentoProvider:FechamentoProvider) {
      super(navCtrl, navParams, alertCtrl, loadingCtrl, toastCtrl);
  }

  ionViewDidLoad() {
    this.contas = this.fechamentoProvider.getAllContasSemFechamento();
  }

  remove(c:any){
    this.fechamentoProvider.delete(c).toPromise().then((data)=>{
      this.mostrarMsg(data.message);
      this.contas = this.fechamentoProvider.getAllContasSemFechamento();
    });
  }

  texto(formaPagamento:string):string{
    switch (formaPagamento){
      case 'dinheiro' :
        return 'Dinheiro.';
      case 'credito' :
        return 'Cartão de Crédito.';
      case 'debito' :
        return 'Cartão de Débito.';
      case 'contaDividida' :
        return 'Conta Dividida.';
      default :
        return '';
    }
  }

}
