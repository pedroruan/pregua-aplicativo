import { Component } from '@angular/core';
import { IonicPageModule, NavController, NavParams, AlertController, LoadingController, ToastController, Toast, Events } from 'ionic-angular';
import { BasePage } from "../base-page";
import { ResumoPage } from "../resumo/resumo";
import { Observable } from 'rxjs/Observable';
import { StoreProvider } from '../../providers/product/product';
import { Pedido } from '../../models/pedido.model';
import { Fechamento } from '../../models/fechamento.model';
import { Produto } from '../../models/produto.model';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage extends BasePage {

  fechamento: Fechamento;
  pages: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public productProvider: StoreProvider,
    public events:Events,
    public toastCtrl: ToastController) {
      super(navCtrl, navParams, alertCtrl, loadingCtrl, toastCtrl);
      
      this.init();
      
      events.subscribe('home:init', () => {
        this.init();
      });
  }

  init(){
    this.fechamento = <Fechamento>{};
    this.fechamento.pedidos = [];
    this.fechamento.quantidadeItens = 0;
    this.fechamento.valorTotal = 0;
    this.fechamento.divisaoContas = null;
    this.fechamento.numeroContas = 0;
    this.fechamento.formaPagamento = null;
    this.search();
  }

  getPedido(p:Produto):Pedido{
    let pedido:Pedido = this.fechamento.pedidos.find((pedido:Pedido) => pedido.produto._id == p._id);
    if(!pedido){
      pedido = <Pedido>{};
      pedido.produto = p;
      pedido.quantidade = 0;
      this.fechamento.pedidos.push(pedido);
    }
    return pedido;
  }

  search() {
    this.showLoading();
    this.productProvider.getAllProducts().toPromise().then((data) => {
      this.pages = data;
      this.dismissLoading();
    })
    .catch((err)=>{
      this.dismissLoading();
      let toast: Toast = this.toastCtrl.create({
        message: 'Erro no servidor.',
        duration: 5000,
        position: 'bottom'
      });
      toast.present();
    });
  }
  
  goToResumo() {
    let param: any = {};
    if(this.fechamento.quantidadeItens < 1 || this.fechamento.valorTotal <= 0) {
      super.mostrarMsg('É necessário Informar quais itens foram consumidos');
      return;
    }

    let pedidos = [];
    var i;
    for (i = 0; i < this.fechamento.pedidos.length; i++) {
      if(this.fechamento.pedidos[i].quantidade) {
        pedidos.push(this.fechamento.pedidos[i]);
      }
    }
    param.fechamento = <Fechamento>{};
    Object.assign(param.fechamento, this.fechamento);
    param.fechamento.pedidos = pedidos;
    param.fechamento.subTotal = param.fechamento.valorTotal;
    this.navCtrl.push(ResumoPage, param);
  }

}
