import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController, Loading, ToastController } from 'ionic-angular';

export class BasePage {

    i: number;
    loading: Loading;
    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        public alertCtrl: AlertController,
        public loadingCtrl: LoadingController,
        public toastCtrl: ToastController) {
    }

    showLoading() {
        this.loading = this.loadingCtrl.create({
          content: 'Carregando...'
        });
        this.loading.present();
    }

    dismissLoading() {
        if (this.loading) {
          this.loading.dismiss();
          this.loading = null;
        }
    }

    formatMoney(valor, places, symbol, thousand, decimal) {
        places = !isNaN(places = Math.abs(places)) ? places : 2;
        symbol = symbol !== undefined ? symbol : "R$";
        thousand = thousand || ".";
        decimal = decimal || ",";
        var negative = valor < 0 ? "-" : "";
        this.i = parseInt(valor = Math.abs(+valor || 0).toFixed(places), 10);
        var j = (j = this.i.toString().length) > 3 ? j % 3 : 0;
        return symbol + negative + (j ? this.i.toString().substr(0, j) + thousand : "") + this.i.toString().substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(valor - this.i).toFixed(places).slice(2) : "");
    };

    onlyNumber(str) {
        if(!str) {
            return null;
        }
        return str.toString().replace(/\D/g, '');
    }

    getRealNumber(strValue, divisorDec): number {

        if(!divisorDec) {
            divisorDec = 100;
        }

        if(strValue && (strValue.toString().indexOf('.') !== -1 || strValue.toString().indexOf(',') !== -1)) {
            strValue = this.onlyNumber(strValue);
        }
        else {
            divisorDec = 1;
        }

        if(strValue) {
            return Number((parseInt(strValue,10) / divisorDec).toFixed(3));
        }

        return 0;
    }

    isInt(n) {
        return Number(n) === n && n % 1 === 0;
    }

    onChangePrice(evt) {
        if(this.isInt(evt))
          evt = evt.toString()+'.00';
        evt = this.getRealNumber(evt, null);
        evt = this.formatMoney(evt, 2, "R$ ", ".", ",");
        return evt;
    }

    mostrarMsg(msg: string) {

        let toast = this.toastCtrl.create({
          message: msg,
          duration: 3000,
          position: 'middle'
        });
    
        toast.onDidDismiss(() => {
          this.dismissLoading();
        });
    
        toast.present();
    }

    mostrarErro(msg) {
        let alert = this.alertCtrl.create({
            title: 'Erro',
            message: msg,
            buttons: ['Ok'],
            cssClass: 'custom-alert'
        });
        alert.present();
    }

    public round(number, precision) {
        var factor = Math.pow(10, precision);
        var tempNumber = number * factor;
        var roundedTempNumber = Math.round(tempNumber);
        return roundedTempNumber / factor;
    };

}