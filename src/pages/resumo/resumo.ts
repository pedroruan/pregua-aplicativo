import { Component, ViewChild  } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, ToastController, Events } from 'ionic-angular';
import { BasePage } from "../base-page";
import { Fechamento } from '../../models/fechamento.model';
import { Pedido } from '../../models/pedido.model';
import { DividirContaPage } from "../dividir-conta/dividir-conta";
import { HomePage } from "../home/home";
import { FechamentoProvider } from "../../providers/fechamento/fechamento";
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { PagamentoModalPage } from '../pagamento-modal/pagamento-modal';
import { DividirConta } from '../../models/dividir.conta.model';

@Component({
  selector: 'page-resumo',
  templateUrl: 'resumo.html',
})
export class ResumoPage extends BasePage {

  @ViewChild("content") content: any;

  @ViewChild("caixinhaInput") caixinhaInput;
  @ViewChild("descontoInput") descontoInput;

  fechamento: Fechamento;
  contas: number[];
  btnDinheiro: string;
  contaDividida:boolean = false;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public events: Events,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    public fechamentoProvider: FechamentoProvider) {
    super(navCtrl, navParams, alertCtrl, loadingCtrl, toastCtrl);
    this.fechamento = this.navParams.get('fechamento');
    this.fechamento.divisaoContas = null;
    this.fechamento.numeroContas = 0;
    this.fechamento.valorDesconto = 0;
    this.fechamento.formaPagamento = null;
    this.fechamento.caixinha = false;
    this.fechamento.valorCaixinha = 0;
    this.contas = [];
    var i = 2;
    for (i; i < 21; i++) {
      this.contas.push(i);
    }
  }

  ionViewDidLoad(){
  }

  recalcularValores() {
    if(!this.fechamento.desconto){
      this.fechamento.valorDesconto = 0;
    }

    if (this.fechamento.caixinha) {
      this.fechamento.subTotal = this.round(this.fechamento.subTotal, 2);
      let valor = (this.fechamento.subTotal - this.fechamento.valorDesconto) * 0.1;
      this.fechamento.valorCaixinha = this.round(valor, 2);
    }
    else {
      this.fechamento.valorCaixinha = 0;
    }
    this.ajustarTotal();
  }

  ajustarTotal() {
    this.fechamento.subTotal = this.round(this.fechamento.subTotal, 2);
    this.fechamento.valorCaixinha =  this.round(this.fechamento.valorCaixinha, 2);
    this.fechamento.valorDesconto =  this.round(this.fechamento.valorDesconto, 2);
    this.fechamento.valorTotal = this.fechamento.subTotal + this.fechamento.valorCaixinha - this.fechamento.valorDesconto;
  }

  goToDivirConta() {
    if (!this.fechamento.numeroContas || this.fechamento.numeroContas < 2) {
      super.mostrarMsg('É necessário informar um numero de divisão de contas maior que 1!');
      return;
    }
    let param: any = {};
    param.fechamento = this.fechamento;
    this.navCtrl.push(DividirContaPage, param);
  }

  calcularValorCorrente(p: Pedido) {
    let valor = super.getRealNumber(super.formatMoney(p.produto.valor, 2, "R$ ", ".", ","), null);
    valor = valor * p.quantidade;
    return super.formatMoney(valor, 2, "R$ ", ".", ",");
  }

  calcularTroco() {
    if (this.fechamento.valorPago) {
      let valorPago = this.round(this.fechamento.valorPago, 2);
      if (valorPago < this.fechamento.valorTotal) {
        super.mostrarMsg('O valor pago informado é menor que o valor total da compra!');
        return;
      }
      valorPago = valorPago - this.fechamento.valorTotal;
      this.fechamento.valorTroco = this.round(valorPago, 2);
    }
  }

  selectFormaPagamento(formaPagamento: string) {
    
    let conta:DividirConta = <DividirConta>{};
    conta.valorConta = this.fechamento.valorTotal;
    conta.valorPago = this.fechamento.valorPago;
    conta.valorTroco = this.fechamento.valorTroco;

    let pgtoModal = this.modalCtrl.create(PagamentoModalPage, { conta: conta, formaPagamento: formaPagamento });
    pgtoModal.onDidDismiss(contaPaga =>{
      if(contaPaga){
        this.fechamento.formaPagamento = formaPagamento;
        this.fechamento.valorPago = contaPaga.valorPago;
        this.fechamento.valorTroco = contaPaga.valorTroco;

        if(contaPaga.valorCaixinha > 0){
          this.fechamento.valorCaixinha +=  this.round(contaPaga.valorCaixinha, 2);
        }
        if(contaPaga.valorDesconto > 0){
          this.fechamento.valorDesconto +=  this.round(contaPaga.valorDesconto, 2);
        }
      }
    });
    pgtoModal.present();
  }

  fecharConta() {
    if (!this.fechamento.formaPagamento) {
      super.mostrarMsg('É necessário informar a forma de Pagamento!');
      return;
    }
    this.showLoading();
    this.fechamentoProvider.saveFechamento(this.fechamento).toPromise().then((r) => {
      console.log(r);
      super.mostrarMsg(r.message);
      this.events.publish('home:init');
      this.dismissLoading();
      this.navCtrl.pop();
    }).catch((e) => {
      console.log(e);
      super.mostrarErro((e.error && e.error.message) ? e.error.message : e.message);
      super.dismissLoading();
    });
  }

  scrollTo(element: string) {
    let yOffset = document.getElementById(element).offsetTop;
    this.content.scrollTo(0, yOffset, 200)
  }

}