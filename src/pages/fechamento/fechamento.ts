import { Component } from '@angular/core';
import {  IonicPage, NavController, NavParams, AlertController, LoadingController, ToastController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { BasePage } from "../base-page";
import { FechamentoProvider } from "../../providers/fechamento/fechamento";
import { FechamentoCaixaModel } from '../../models/fechamento.caixa.model';

@Component({
  selector: 'page-fechamento',
  templateUrl: 'fechamento.html',
})
export class FechamentoPage extends BasePage {

  tipo:string;
  fechamentoCaixa: FechamentoCaixaModel;

  produtosVendidos: any[];

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public fechamentoProvider: FechamentoProvider) {
      super(navCtrl, navParams, alertCtrl, loadingCtrl, toastCtrl);
      this.fechamentoCaixa = <FechamentoCaixaModel>{};
      this.search();
  }

  search() {
    this.tipo = 'formaPgto';
    this.showLoading();
    this.fechamentoProvider.getFechamentosAberto().toPromise().then((data) => {
      this.fechamentoCaixa = data;
      this.produtosVendidos = data.produtosVendidos;
      this.dismissLoading();
    });
  }

  fecharCaixa() {
    this.showLoading();
    this.fechamentoProvider.fecharCaixa().toPromise().then((r) => {
      console.log(r);
      super.mostrarMsg(r.message);
      this.navCtrl.popToRoot();
      this.dismissLoading();
    }).catch((e) => {
      console.log(e);
      super.mostrarErro((e.error && e.error.message) ? e.error.message : e.message);
      super.dismissLoading();
    });
  }

}
