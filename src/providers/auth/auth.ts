import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {tap} from 'rxjs/operators/tap';

import { ReplaySubject, Observable } from 'rxjs';
import {Storage} from "@ionic/storage";
import {JwtHelperService} from "@auth0/angular-jwt";

import {config} from "../../config";

@Injectable()
export class AuthProvider {
  
  authUser = new ReplaySubject<any>(1);
  user = new ReplaySubject<any>(1);
  api = config.HOST +'/users'

  constructor(
    public http: HttpClient, 
    private storage: Storage, 
    private jwtHelper: JwtHelperService
  ) {
    this.storage.get(config.JWT_TOKEN_NAME).then(jwt => {
      this.authUser.next(jwt);
    });
  }

  checkLogin() {
    this.storage.get(config.JWT_TOKEN_NAME).then(jwt => {
      if (jwt && !this.jwtHelper.isTokenExpired(jwt)) {
        this.http.get(`${config.HOST}/users/auth/validate-token`)
        .subscribe(() => this.authUser.next(jwt),
          (err) => this.storage.remove(config.JWT_TOKEN_NAME)
                  .then(() => this.authUser.next(null))
        );
      }
      else {
        this.storage.remove(config.JWT_TOKEN_NAME).then(() => this.authUser.next(null));
      }
    });
  }

  login(email, password): Observable<any> {
    return this.http.post(`${this.api}/login`, {'email':email, 'password':password }, {responseType: 'text'})
      .pipe(
        tap(jwt => {
          this.handleUser(JSON.stringify(JSON.parse(jwt).user));
          this.handleJwtResponse(JSON.parse(jwt).token);
        }
      ));
  }

  logout() {
    this.storage.remove(config.JWT_TOKEN_NAME).then(() => this.authUser.next(null));
    this.storage.remove('user').then(() => this.user.next(null));
  }

  private handleJwtResponse(jwt: string) {
    return this.storage.set(config.JWT_TOKEN_NAME, jwt)
      .then(() => this.authUser.next(jwt))
      .then(() => jwt);
  }

  private handleUser(user: string) {
    return this.storage.set('user', user).then(()=> this.user.next(user));
  }

  getUser():any {
    return this.storage.get('user').then( 
      (user) => JSON.parse(user),
      erro => false
    );
  }
}
