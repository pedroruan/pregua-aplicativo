import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import {config} from "../../config";
import {BaseProvider} from "../base";
import {Storage} from "@ionic/storage";


@Injectable()
export class StoreProvider extends BaseProvider {
  private apiUrl = config.HOST +'/product';

  constructor(public http: HttpClient,
              public storage: Storage) {
      super(storage);
  }

  getAllProducts(): Observable<any> {
      return this.http.get(this.apiUrl);
  }

  getProduct(id: any): Observable<any> {
      return this.http.get(this.apiUrl + '/' + id);
  }
}
