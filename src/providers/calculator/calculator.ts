import { Injectable } from '@angular/core';

@Injectable()
export class CalculatorProvider {
  public discount_store: number;
  public value_store: number;
  private discount_app = 33;
  private value_real_discount: number;
  
  constructor() {
   
  }
  
  getValueDiscounted(): number {
    return this.value_store * this.discount_store/100;
  }

  getValueDiscountedApp(): number {
    let valorRealDiscounted: number = this.getValueDiscounted(); 
    return valorRealDiscounted * this.discount_app/100;
  }

  getSavedValue(): number {
    let valorRealDiscounted: number = this.getValueDiscounted(); 
    return this.getValueDiscounted() - this.getValueDiscountedApp();
  }

  getValueToPay(): number {
    return this.value_store - this.getValueDiscounted();
  }

  getFormatedValueDiscountApp(): string{
    let value = Math.round(this.getValueDiscountedApp()*100) / 100;
    return this.formatedNumber(value);
  }

  getFormatedSavedValue(): string{
    let value = Math.round(this.getSavedValue()*100) / 100;
    return this.formatedNumber(value);
  }
  getFormatedValueToPay(): string{
    let value = Math.round(this.getValueToPay()*100) / 100;
    return this.formatedNumber(value);
  }

  formatedNumber(value):string{
    let valor = Math.round(value*100) / 100;
    let decimal = valor.toString().replace('.',',');
    
    if(decimal.search(',') > 0){
      const arrayValor = decimal.split(',');
      if( arrayValor[1].length < 2 ){
        decimal = arrayValor[0] +','+ arrayValor[1];
        decimal += '0';
      }else{
        decimal = arrayValor[0] +','+ arrayValor[1];
      }
    }else{
      decimal += ',00'
    }
    
    return decimal;
  }

}
