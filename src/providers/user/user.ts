import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { config } from "../../config";
import { Observable } from 'rxjs';
import { Company } from '../../models/company.model';

/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserProvider {
  apiUrl = config.HOST + '/company';

  constructor(public http: HttpClient) {
    console.log('Hello UserProvider Provider');
  }

  getCompany(id: any): Observable<Company> {
    return <Observable<Company>> this.http.get(this.apiUrl + '/' + id);
  }

}
