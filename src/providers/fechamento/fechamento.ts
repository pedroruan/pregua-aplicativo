import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import {config} from "../../config";
import {BaseProvider} from "../base";
import {Storage} from "@ionic/storage";
import { Fechamento } from '../../models/fechamento.model';


@Injectable()
export class FechamentoProvider extends BaseProvider {
    private apiUrl = config.HOST +'/fechamento';

    constructor(public http: HttpClient,
                public storage: Storage) {
        super(storage);
    }

    getAllContasSemFechamento(): Observable<any> {
        return this.http.get(this.apiUrl+'/contas');
    }

    getAllFechamentos(): Observable<any> {
        return this.http.get(this.apiUrl);
    }

    getFechamento(id: any): Observable<any> {
        return this.http.get(this.apiUrl + '/' + id);
    }

    getFechamentosAberto(): Observable<any> {
        return this.http.get(this.apiUrl + '/all/abertos');
    }

    saveFechamento(fechamento: Fechamento): Observable<any> {
        return this.http.post(this.apiUrl, fechamento).map((response: Response) => {
            return response;
        }).catch((e) => { throw e; });
    }

    delete(fechamento: Fechamento):Observable<any>{
        return this.http.delete(this.apiUrl + '/' + fechamento._id);
    }

    fecharCaixa(): Observable<any> {
        return this.http.put(this.apiUrl, null).map((response: Response) => {
            return response;
        }).catch((e) => { throw e; });
    }
}