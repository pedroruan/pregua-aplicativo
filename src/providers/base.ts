import { Response, RequestOptions, Headers, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs';
import { AuthProvider } from '../providers/auth/auth';
import {Storage} from "@ionic/storage";
import {config} from "../config";

export abstract class BaseProvider {

    constructor(public storage: Storage) {

    }

    protected buildOpts(): RequestOptions {
        let opts = new RequestOptions();
        this.storage.get(config.JWT_TOKEN_NAME).then(jwt => {
            opts.headers = new Headers();
            opts.headers.append('Authorization', 'Bearer '+jwt);
        });
        return opts;
    }
}