export const config = {
    // HOST : "http://localhost:3000",
    //HOST : "http://10.42.0.18:3000",
    //HOST : "http://192.168.15.3:3000",
    
    HOST:'http://45.55.144.89:3000',
    JWT_TOKEN_NAME: 'token',
    EMAIL_REGEXP : /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    // Passwords should be at least 8 characters long and should contain one number, one character and one special character.
    PASSWORD_REGEXP : /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
}
