import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import {Storage, IonicStorageModule} from "@ionic/storage";
import {NgxPaginationModule} from 'ngx-pagination';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ResumoPage } from '../pages/resumo/resumo';
import { JWT_OPTIONS, JwtModule } from '@auth0/angular-jwt';
import { AuthProvider } from '../providers/auth/auth';
import { UserProvider } from '../providers/user/user';
import { StoreProvider } from '../providers/product/product';
import { FechamentoProvider } from '../providers/fechamento/fechamento';
import {HttpModule} from '@angular/http';
import {HttpClientModule} from "@angular/common/http";
import { DividirContaPage } from "../pages/dividir-conta/dividir-conta";
import { FechamentoPage } from "../pages/fechamento/fechamento";
import { IntroPage } from '../pages/intro/intro';
import { HeaderComponent } from '../components/header/header';
import { SobrePage } from '../pages/sobre/sobre';
import { InfoComponent } from '../components/info/info';
import { PagamentoModalPage } from '../pages/pagamento-modal/pagamento-modal';
import { ReversePipe } from '../pipes/reverse/reverse';
import { ContasFechadasPage } from '../pages/contas-fechadas/contas-fechadas';
import { ProdutoItemComponent } from '../components/produto-item/produto-item';

export function jwtOptionsFactory(storage: Storage) {
  return {
    tokenGetter: () =>
      storage.get('token'),
      whitelistedDomains: [ 'localhost:8100', 'notes.api', 'localhost:3000', '45.55.144.89:3000', '10.42.0.18:3000']
  }
}


@NgModule({
  declarations: [
    MyApp,
    IntroPage,
    HomePage,
    ResumoPage,
    SobrePage,
    DividirContaPage,
    FechamentoPage,
    HeaderComponent,
    InfoComponent,
    ProdutoItemComponent,
    PagamentoModalPage,
    ContasFechadasPage,
    ReversePipe
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    HttpClientModule,
    NgxPaginationModule,
    IonicStorageModule.forRoot(),
    JwtModule.forRoot({
      jwtOptionsProvider: {
        provide: JWT_OPTIONS,
        useFactory: jwtOptionsFactory,
        deps: [Storage]
      }
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    IntroPage,
    HomePage,
    SobrePage,
    ResumoPage,
    DividirContaPage,
    FechamentoPage,
    PagamentoModalPage,
    ContasFechadasPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider,
    UserProvider,
    StoreProvider,
    FechamentoProvider
  ]
})
export class AppModule {}
