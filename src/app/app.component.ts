import { Nav, Slides, MenuController, LoadingController, ToastController, Toast, Loading, IonicApp } from 'ionic-angular';
import { ViewChild } from '@angular/core';
import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { finalize } from 'rxjs/operators/finalize';

import { AuthProvider } from '../providers/auth/auth';

import { HomePage } from '../pages/home/home';
import { FechamentoPage } from '../pages/fechamento/fechamento';
import { IntroPage } from '../pages/intro/intro';
import { NavController } from 'ionic-angular/navigation/nav-controller';
import { Events } from 'ionic-angular/util/events';
import { SobrePage } from '../pages/sobre/sobre';
import { App } from 'ionic-angular/components/app/app';
import { Company } from '../models/company.model';
import { UserProvider } from '../providers/user/user';
import { Observable } from 'rxjs';
import { ContasFechadasPage } from '../pages/contas-fechadas/contas-fechadas';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  navCtrl: NavController;
  loading: Loading;
  rootPage: any = IntroPage;
  company: Company;

  constructor(platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    public menuCtrl: MenuController,
    private authProvider: AuthProvider,
    private userProvider: UserProvider,
    public loadingCtrl: LoadingController,
    public events: Events,
    public app: App,
    public toastCtrl: ToastController) {

    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
      this.authProvider.authUser.subscribe((token) => {
        if (token) {
          this.authProvider.getUser().then(user => {
            this.userProvider.getCompany(user.idEmpresa).toPromise()
            .then((company: Company) => {
              this.company = company;
            })
            .catch((err)=>{
              console.log(err);
              this.company = null;
            });
          });
          this.rootPage = HomePage;
        } else
          this.rootPage = IntroPage;
      });
    });
  }

  goTo(page: any) {
    this.navCtrl = this.app.getActiveNav();
    if (this.navCtrl.getActive().instance instanceof page)
      return
    else
      this.navCtrl.push(page);
  }

  home() {
    this.navCtrl = this.app.getActiveNav();
    this.events.publish('home:init');
    this.navCtrl.popToRoot();
  }

  contas() {
    this.goTo(ContasFechadasPage);
  }

  fechamento() {
    this.goTo(FechamentoPage);
  }

  sobre() {
    this.goTo(SobrePage);
  }

  logout() {
    this.authProvider.logout();
  }

}

